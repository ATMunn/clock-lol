# [clock lol](https://atmunn.gitlab.io/clock-lol/)

A nice clock for your browser.

## How to use

* Click on the time to switch between 12-hour and 24-hour formats.
* Click on the date to switch between compact and expanded formats.
* Click the button in the top-right corner to toggle fullscreen.
* Click the button in the bottom-right corner to enter low distraction mode (no moving colors, seconds are hidden)
