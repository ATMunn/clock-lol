let darkMode = false;
let shortDate = false;
let animPlaying = false;
let fullscreen = false;
let dumbFormat = false;
let version = "1.2";

function init() {
    if(localStorage.getItem("darkMode") == "true") {
        darkMode = true;

        let clock = document.getElementById("clock");
        let icon = document.getElementById("dm-icon");
        let tooltip = document.getElementById("dm-tooltip");

        document.body.classList.add("darkmode");
        clock.classList.add("darkmode");

        icon.classList.toggle("far");
        icon.classList.toggle("fa-moon");
        icon.classList.toggle("fas");
        icon.classList.toggle("fa-times");

        tooltip.innerHTML="exit low distraction mode";
    }
    if(localStorage.getItem("shortDate") == "true")
        shortDate = true;
    if(localStorage.getItem("dumbFormat") == "true")
        dumbFormat = true;

    time(); 
    gradStart(); 
    setSVGHeight();

    if(localStorage.getItem("version") && localStorage.getItem("version") !== version) {
        openDialog("changelog")
    }
    localStorage.setItem("version", version)
}

function time() {
    let date = new Date();

    let second = ("0"+ date.getSeconds()).slice(-2);
    let minute = ("0"+ date.getMinutes()).slice(-2);
    let hour = ("0"+ date.getHours()).slice(-2);

    let days = ["Sunday", "Munnday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

    let weekday = date.getDay();
    let month = date.getMonth();
    let weekdayw = days[weekday];
    let monthw = months[month];
    let day = date.getDate();
    let year = date.getFullYear();

    let time = document.getElementById("time");
    if(dumbFormat) {
        if(hour > 12) {
            time.classList.add("pm");
            time.classList.remove("am");
            hour -= 12;
        }
        else if(hour == 12) {
            time.classList.add("pm");
            time.classList.remove("am");
        }
        else {
            time.classList.add("am");
            time.classList.remove("pm");
        }

        if (darkMode)
            time.innerHTML = hour + ":" + minute; // no seconds in low distraction mode
        else
            time.innerHTML = ("0"+ hour).slice(-2) + ":" + minute + ":" + second;
    }
    else {
        if (darkMode)
            time.innerHTML = hour + ":" + minute; // no seconds in low distraction mode
        else
            time.innerHTML = hour + ":" + minute + ":" + second;
    }

    let dateElement = document.getElementById("date");
    if (shortDate)
        dateElement.innerHTML = (month+1) + " / " + day + " / " + year;
    else
        dateElement.innerHTML = weekdayw + ", " + monthw + " " + day + ", " + year;
}

function play() {
    let transition = document.getElementById("transition");
    let clock = document.getElementById("clock");
    let icon = document.getElementById("dm-icon");
    let tooltip = document.getElementById("dm-tooltip");

    if(!animPlaying){
        setSVGHeight();
        transition.classList.add("play");
        animPlaying = true;
        if (darkMode) {
            let _ = setTimeout(() => {
                darkMode = false;
                localStorage.setItem("darkMode", "false");
                time();

                document.body.classList.remove("darkmode");
                clock.classList.remove("darkmode");

                icon.innerHTML = "dark_mode"

                tooltip.innerHTML="enter low distraction mode";

                gradStart();
            }, 550);  
        }
        else {
            let _ = setTimeout(() => {
                darkMode = true;
                localStorage.setItem("darkMode", "true");
                time();

                document.body.classList.add("darkmode");
                clock.classList.add("darkmode");

                icon.innerHTML = "light_mode"

                tooltip.innerHTML="exit low distraction mode";

            }, 550);
        }

        let __ = setTimeout(() => {
            transition.classList.remove("play");
            animPlaying = false;
        }, 2000);
    }
}

function toggleDate() {
    if(shortDate) {
        shortDate = false;
        localStorage.setItem("shortDate", "false");
    }
    else {
        shortDate = true;
        localStorage.setItem("shortDate", "true");
    }
    time();
}

function toggleFormat() {
    if(dumbFormat) {
        dumbFormat = false;
        localStorage.setItem("dumbFormat", "false");

        let time = document.getElementById("time");
        time.classList.remove("am");
        time.classList.remove("pm");
    } else {
        dumbFormat = true;
        localStorage.setItem("dumbFormat", "true");
    }

    time();
}

function gradStart() {
    let body = document.body;

    let secs = new Date().getSeconds();

    body.style.animationDelay = (0-secs)+"s";
}

function openFullscreen() {
    let doc = document.documentElement;

    if (doc.requestFullscreen) {
        doc.requestFullscreen();
    } else if (doc.mozRequestFullScreen) { /* Firefox */
        doc.mozRequestFullScreen();
    } else if (doc.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        doct.webkitRequestFullscreen();
    } else if (doc.msRequestFullscreen) { /* IE/Edge */
        doc.msRequestFullscreen();
    }
}

function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
    }
}

function toggleFullscreen() {
    let icon = document.getElementById("fs-icon");
    let tooltip = document.getElementById("fs-tooltip");

    if(fullscreen) {
        closeFullscreen();
        fullscreen = false;
        tooltip.innerHTML = "enter fullscreen";
        icon.innerHTML = "fullscreen"
    }
    else {
        openFullscreen();
        fullscreen = true;
        tooltip.innerHTML = "exit fullscreen";
        icon.innerHTML = "fullscreen_exit"
    }

}

function setSVGHeight() {
    let vh = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    let svg = document.getElementById("transition");
    let rect = document.getElementById("transition-rect");

    svg.setAttribute("height", vh*1.3);
    rect.setAttribute("height", vh*1.3);
}

function openDialog(name) {
    let dialog = document.getElementById(name);
    let box = document.getElementById(`${name}-box`);

    dialog.classList.add("visible");

    dialog.onclick = function() {
        dialog.classList.remove("visible");
    }
    box.onclick = function(){
        event.stopPropagation();
    }
}

window.setInterval(time, 1000);
window.onresize = setSVGHeight;